# Using this template

## What you need:

- reasonably up-to-date nodejs
- yarn (`npm -g install yarn`)

## Setting up

- `yarn install`

## Running

- `yarn start`
