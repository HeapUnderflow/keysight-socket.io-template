const app = require("express");
const http = require("http").Server(app);
const io = require("socket.io")(http);

function pushCamera(data) {
    // console.debug(`${Date.now()} - pushing camera data`, data);
    io.emit("command", "camera:" + JSON.stringify(data));
}

function posOnTime(upTo) {
    return (Date.now() / 8) % upTo;
}

io.on('connection', function (socket) {
    console.log(`new client [id=${socket.id}, addr=${socket.handshake.address}]`);
    setInterval(function () {
        pushCamera(
            {
                "x": -58.58 + posOnTime(100),
                "y": -52.48,
                "z": 90,
                "tilt":-52.61,
                "rotate":0,
                "roll": 0, // -38.03,
                "localRoll": 90, // 90,
                "FOV":65.00
            }
        );
    }, 1000 / 60);
})

http.listen(3000, function(){
    console.log('listening on *:3000');
});
